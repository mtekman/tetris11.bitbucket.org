
var trace_ray = new PIXI.Graphics();
// trace_ray is used to draw all rays in a scene

var color_map = { 
	"red"  : 0xff0000,
	"green": 0x00ff00,
	"blue" : 0x0000ff
}

// Temp
var start = [0,0], 
	end = [0,0],
	result = new p2.RaycastResult(),
	hitPoint = p2.vec2.create();

var raycastOptions = {};
/* The three types of rays:
   Closest = drawHitResult regardless, hitResult = First Hit
   ANY  = drawHitResult regardless, hitResult = Last Hit
   ALL = drawHitResult regardless, and on ALL hit callbacks too
         otherwise it chooses any random hit to draw.
*/
var rayClosest = new p2.Ray({mode: p2.Ray.CLOSEST});
var rayAny = new p2.Ray({mode: p2.Ray.ANY});
var rayAll = new p2.Ray({
	mode: p2.Ray.ALL,
	callback: function(result){
		drawRayResult("green", result, rayAll);
	}
});


function renderRays(){
	clearRays();
	drawRays();
}

function drawRayResult( color, result, ray )
{
	result.getHitPoint(hitPoint, ray);
	
	// Draw hit point
	var lcol = color_map[color]
	
	if(result.hasHit()){
		trace_ray.moveTo( hitPoint[0], hitPoint[1] )
		trace_ray.arc(hitPoint[0],hitPoint[1],0.1,0,2*Math.PI);
	}

	// Draw hit normal
	trace_ray.moveTo(hitPoint[0], hitPoint[1]);
	trace_ray.lineTo(
		hitPoint[0] + result.normal[0],
		hitPoint[1] + result.normal[1]
	);
}



function drawRays(){
    start[0] = -3;
    start[1] = Math.sin(world.time) * 4;
    end[0] = 5;
    end[1] = Math.sin(world.time);

    // All
    p2.vec2.copy(rayAll.from, start);
    p2.vec2.copy(rayAll.to, end);
    rayAll.update();
    drawRay( "green", start, end);
    result.reset();
    world.raycast(result, rayAll); // drawRayResult
    drawRayResult( "green", result, rayAll )

    start[1] += 0.5;
    end[1] += 0.5;


    // Closest
    p2.vec2.copy(rayClosest.from, start);
    p2.vec2.copy(rayClosest.to, end);
    rayClosest.update();
    drawRay( "blue" , start , end )
    result.reset();
    world.raycast(result, rayClosest);
    drawRayResult("blue", result, rayClosest);

    start[1] += 0.5;
    end[1] += 0.5;

    // Any
    p2.vec2.copy(rayAny.from, start);
    p2.vec2.copy(rayAny.to, end);
    rayAny.update();
    drawRay( "red", start, end);
    result.reset();
    world.raycast(result, rayAny);
    drawRayResult("red", result, rayAny);
}

// Little helpers

function drawRay(color, start, end)
{
	var lcolor = color_map[color];
	
	trace_ray.lineStyle( 0.02, lcolor, 1);
	trace_ray.moveTo(start[0], start[1]);
	trace_ray.lineTo(end[0]  , end[1]  );
}

function clearRays(){
	trace_ray.clear()
}