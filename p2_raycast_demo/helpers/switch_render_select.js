var select_box = document.getElementById('render_style');
select_box.onchange = changeRender;



function initSelectBoxValues()
{   
    for (var i=1; i< Object.keys(PIXI.RENDERER_TYPE).length; i++)
    {
	var value  = Object.keys(PIXI.RENDERER_TYPE)[i],
	    option = document.createElement('option');
	
	option.appendChild( document.createTextNode( value ) )
	option.value = value;

	select_box.appendChild( option );
    }    
}



function setSelectBox( value )
{
    select_box.value = value;
}



function changeRender()
{
    var href = window.location.href.split('?=')[0];

    if (select_box.value ===  "WEBGL" ){
	href = href + '?=use_webgl' // actual message is irrelevant
	window.location = href
    }
    else {
	window.location  = href
    }
}


initSelectBoxValues();
