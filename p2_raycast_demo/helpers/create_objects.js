//Create objects for both P2 and Pixi

var entities = {}; // global accessor of objects

var lthick = 0.05

var planeProps = { position: [3,0], angle: Math.PI / 3},
    circleProps = { mass:1, position: [0,-1], angularVelocity:1 },
    boxProps = { mass: 1, position: [0,2], angularVelocity: 1, angularDamping: 0 },
    vertexProps = { mass: 1, position: [1,0], angle: Math.PI / 3, angularVelocity: 1 },
    capsProps = { mass:1, position:[-1.5,0], angularVelocity:1, angularDamping: 0 };



// Scene = p2.world and pixi.container

function addToScene(){
	
	for (var i=0; i < arguments.length; i++)
	{
		var obj = arguments[i];
		entities[obj.name] = obj;

		world.addBody(obj.physics);
		container.addChild(obj.graphics);
	}
}



// Common objects to exist in both worlds

function createBox( name, width, height, props){
	props = props || boxProps
	
    var boxShape = new p2.Box({ width: width, height: height });
    var boxBod = new p2.Body(props);

    boxBod.addShape(boxShape);

    // Draw the box.
    var graphics = new PIXI.Graphics();
    graphics.lineStyle( lthick, 0xaa0000, 1);
    graphics.drawRect(-width/2, -height/2, width, height);

    return { name: name, physics: boxBod, graphics: graphics };
}

function createCircle( name, radius, props ){
	props = props || circleProps

    var circleShape = new p2.Circle({ radius: radius }),
	circleBody = new p2.Body( props );
    
    circleBody.addShape(circleShape);

    var graphics = new PIXI.Graphics();
    graphics.lineStyle( lthick, 0xaa0000, 1);
    graphics.drawCircle( 0, 0, radius );

    return { name: name, physics: circleBody, graphics: graphics }

}

function createPlane( name, props ){
	// Add a plane
	props = props || planeProps

	var planeShape = new p2.Plane(),
		planeBody = new p2.Body(props);

	planeBody.addShape(planeShape);
	
	var graphics = new PIXI.Graphics();
	graphics.lineStyle( lthick, 0x000000, 1);
	graphics.moveTo(-100, 0 )
	graphics.lineTo(100, 0 );
	
    return { name: name, physics: planeBody, graphics: graphics }
}

function createConvex( name, size, angles, props ){
	angles = angles || 5
	props = props || vertexProps;
	
	var vertices = [[ size, 0 ]];
	
	// Draw
	var graphics = new PIXI.Graphics();
		graphics.lineStyle( lthick, 0x000000, 1);
		graphics.moveTo( vertices[0][0], vertices[0][1] )
    
	for(var i=1; i < angles+1; i++)
	{
		var a = ( 2 * Math.PI / angles) * i,
			vertex = [
				size*Math.cos(a),
				size*Math.sin(a)
			]; // Note: vertices are added counter-clockwise

		vertices.push(vertex);
		graphics.lineTo( vertex[0], vertex[1] );
	}
    
	var convexShape = new p2.Convex({ vertices: vertices }),
		convexBody = new p2.Body( props );
    
    convexBody.addShape(convexShape);

    return { name: name, physics: convexBody, graphics: graphics }
}

function createCapsule( name, len, radius, props ){
	props = props || capsProps;
	
	var capsuleShape = new p2.Capsule({ length: len, radius: radius }),
		capsuleBody = new p2.Body( props );

	capsuleBody.addShape(capsuleShape);
	
	var ctx = new PIXI.Graphics();
	ctx.lineStyle( lthick, 0x000000, 1);
	
    // Draw circles at ends
	ctx.arc( len/2, 0, radius, -Math.PI/2, Math.PI/2);
    ctx.moveTo( len/2, -radius);
    ctx.lineTo( -len/2, -radius);
	ctx.moveTo( len/2 , radius);
	ctx.arc( -len/2, 0, radius,  Math.PI/2, -Math.PI/2);
	
	return { name: name, physics: capsuleBody, graphics: ctx};
}

function createRay( name, props ){
	props = props || rayProps;
	
	var planeShape = new p2.Plane(),
		planeBody = new p2.Body(props);

	planeBody.addShape(planeShape);
	
	var ctx = new PIXI.Graphics();
	return { name: name, physics: planeBody, graphics: ctx}
}

