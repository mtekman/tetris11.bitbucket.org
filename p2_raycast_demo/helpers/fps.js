
var fps_lastDate = new Date,
    fps_span = document.getElementById('fps'),
    fps_printloop = 10,
    fps_updatecount = 0,
    fps_average = 0


function fps_log(){
    // Calculate FPS
    var fps_newDate = new Date;
    var fps = 1000 / (fps_newDate - fps_lastDate)
    fps_average += fps

    if (fps_updatecount ++ > fps_printloop){
	fps_average = fps_average / (fps_printloop + 1)
	fps_span.innerHTML = parseInt(fps_average)
	fps_updatecount = 0
	fps_average = 0
    }

    // Store fps
    fps_lastDate = fps_newDate
}
