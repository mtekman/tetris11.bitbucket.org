var renderer, container

function useCanvas()  // Determined by url
{
    return (window.location.href.split('?=').length !== 2);
}

function pixi_init(){

    var render_props = { transparent: true, antialias: true };
     
    if (useCanvas()){
	renderer = new PIXI.CanvasRenderer(600,600, render_props)
    }
    else renderer = new PIXI.WebGLRenderer(600,600, render_props)

    //    div_cont.innerHTML = renderer.view
    document.body.appendChild( renderer.view )
	
    container = new PIXI.Container();
    container.position.x =  renderer.width/2; // center at origin
    container.position.y =  renderer.height/2;
    container.scale.x =  40;  // zoom in
    container.scale.y = -40; // Note: we flip the y axis to make "up" the physics "up"

    var current_renderer_type = Object.keys( PIXI.RENDERER_TYPE )[ renderer.type ]
    console.log("Current Renderer:", current_renderer_type );
    setSelectBox( current_renderer_type );
}


function renderObjects(){
    renderer.render(container)

    // Draw all bodies
    drawBox();
    drawCircle();
    drawPlane();
    drawConvex();
    drawCapsule();
}

function drawBox()    {drawEntity("box")    ;}
function drawCircle() {drawEntity("circle") ;}
function drawPlane()  {drawEntity("plane")  ;}
function drawConvex() {drawEntity("convex") ;}
function drawCapsule(){drawEntity("capsule");}

function drawEntity( name )
{
	var ent = entities[ name ],
		body = ent.physics,
		grfx = ent.graphics;

	var x = body.position[0],
		y = body.position[1];

	grfx.x = x;
	grfx.y = y;
	grfx.rotation = body.angle;
}
