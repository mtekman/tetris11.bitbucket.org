
var world;

function init(){

    pixi_init()

    // Init p2.js
    world = new p2.World({gravity: [0, 0]});
    
    // Create Environment
    var box     = createBox("box", 2, 1),
	circle  = createCircle( "circle", 0.5),
	plane   = createPlane( "plane" ),
	convex  = createConvex("convex", 0.5, 5),
	capsule = createCapsule("capsule", 1, 0.5 );
    
    // Scene means p2.world and pixi.container
    addToScene(box, circle, plane, convex, capsule);
	
    // Rays are not objects as such, so we 
    container.addChild(trace_ray);
}


// Animation loop
var timeStep = 1 / 60,
    maxSubSteps = 1,
    lastTime;


function animate(t)
{
    requestAnimationFrame(animate);

    // Time stepping
    // http://www.html5gamedevs.com/topic/15990-updating-the-world-according-to-time-with-p2js/
    // http://schteppe.github.io/p2.js/docs/classes/World.html#method_step

    // This works well
    var dt = t !== undefined && lastTime !== undefined ? t / 1000 - lastTime : 0;



    world.step( timeStep , dt , maxSubSteps);
    lastTime = t / 1000;


    // This not so much
/*    requestAnimationFrame(animate);

    var timeSeconds = t / 1000;
    lastTime = lastTime || timeSeconds;

    var timeSinceLastCall = timeSeconds - lastTime;
    world.step(timeStep, timeSinceLastCall, maxSubSteps);
	*/

    // Render scene
    renderRays();
    renderObjects();

    // Metrics
    fps_log()
}


init();
animate();
